

const documentElement = document.documentElement || document.querySelector('html')


let prevState

function disableScroll() {
  if(documentElement) {
    prevState = documentElement.style.overflowY;
    documentElement.style.overflowY = 'hidden';
  }
}

function enableScroll() {
  if(documentElement) {
    documentElement.style.overflowY = prevState || '';
    prevState = null;
  }
}

export default {
  documentElement,
  disableScroll,
  enableScroll,
}