import DocumentElement from "./DocumentElement";

class Drawer {
  constructor({
    containerSelector,
    buttonSelector,
    overlaySelector,
    contentSelector = '.drawer__container',
    events = {
      onDrawerOpen: () => {},
      onDrawerClose: () => {},
    },
    threshold = 0.25,
    direction = 'left',
    enableEdgeOpen = true,
    cancelDragElements = [],
  }) {

    this._threshold = threshold
    this._direction = direction
    this._cancelDragElements = cancelDragElements;
    this.events = events;
    this.menu = document.querySelector(containerSelector)
    this.button = document.querySelector(buttonSelector)
    this.menuOverlay = document.querySelector(overlaySelector)

    if (this.menu) {
      this.menuContent = this.menu.querySelector(contentSelector)
      if(enableEdgeOpen) {
        this.swipeHandler = document.createElement('div')
        this.swipeHandler.setAttribute('style',  `
          position: fixed;
          top: 0;
          ${direction}: 0;
          bottom: 0;
          width: 20px;
          z-index: 500;
          background-color: red;
          opacity: 0;
        `)
        document.body.appendChild(this.swipeHandler)
      }
      
    }

    this.onMenuOverlayClick = this.onMenuOverlayClick.bind(this)
    this.onMenuButtonClick = this.onMenuButtonClick.bind(this)
    this.onWindowResize = this.onWindowResize.bind(this)

    this.onTouchStart = this.onTouchStart.bind(this)
    this.onTouchMove = this.onTouchMove.bind(this)
    this.onTouchCancel = this.onTouchCancel.bind(this)
    this.onTouchEnd = this.onTouchEnd.bind(this)

    this.onWindowTouchStart = this.onWindowTouchStart.bind(this)
    this.onWindowTouchMove = this.onWindowTouchMove.bind(this)
    this.onWindowTouchCancel = this.onWindowTouchCancel.bind(this)
    this.onWindowTouchEnd = this.onWindowTouchEnd.bind(this)

    const { lg } = window.BRAKE_POINTS
    this.brakePoint = lg;
    if (window.innerWidth <= this.brakePoint) {
      this.addEventListeners();
    }
    
    window.addEventListener('resize', this.onWindowResize)
  }

  addEventListeners() {
    if(this.added) {
      return
    }
    this.added = true;

    if (this.isOpen()) {
      DocumentElement.disableScroll()
    }
    
    if (this.menuOverlay) {
      this.menuOverlay.addEventListener('click', this.onMenuOverlayClick)
    }

    if (this.button) {
      this.button.addEventListener('click', this.onMenuButtonClick)
    }

    if(this.accountButton) {
      this.accountButton.addEventListener('click', this.onAccountButtonClick)
    }

    if(this.swipeHandler && !Drawer.bodyEventAdded) {
      window.addEventListener('touchstart', this.onWindowTouchStart, { passive: true });
      window.addEventListener('touchmove', this.onWindowTouchMove, { passive: true });
      window.addEventListener('touchcancel', this.onWindowTouchCancel, { passive: true });
      window.addEventListener('touchend', this.onWindowTouchEnd, { passive: true });
    }

    if(this.menu && this.menuContent) {
      this.menu.addEventListener('touchstart', this.onTouchStart, { passive: true });
      this.menu.addEventListener('touchmove', this.onTouchMove, { passive: true });
      this.menu.addEventListener('touchcancel', this.onTouchCancel, { passive: true });
      this.menu.addEventListener('touchend', this.onTouchEnd, { passive: true });
    }
  }

  removeEventListeners() {
    if(!this.added) {
      return
    }
    this.added = false;

    DocumentElement.enableScroll()
    
    if (this.menuOverlay) {
      this.menuOverlay.removeEventListener('click', this.onMenuOverlayClick)
    }

    if (this.button) {
      this.button.removeEventListener('click', this.onMenuButtonClick)
    }

    if(this.accountButton) {
      this.accountButton.removeEventListener('click', this.onAccountButtonClick)
    }

    if(this.swipeHandler && Drawer.bodyEventAdded) {
      window.removeEventListener('touchstart', this.onWindowTouchStart, { passive: true });
      window.removeEventListener('touchmove', this.onWindowTouchMove, { passive: true });
      window.removeEventListener('touchcancel', this.onWindowTouchCancel, { passive: true });
      window.removeEventListener('touchend', this.onWindowTouchEnd, { passive: true });
    }

    if(this.menu && this.menuContent) {
      this.menu.removeEventListener('touchstart', this.onTouchStart, { passive: true });
      this.menu.removeEventListener('touchmove', this.onTouchMove, { passive: true });
      this.menu.removeEventListener('touchcancel', this.onTouchCancel, { passive: true });
      this.menu.removeEventListener('touchend', this.onTouchEnd, { passive: true });
    }
  }

  onWindowResize(event) {
    if (window.innerWidth > this.brakePoint) {
      this.removeEventListeners()
      return
    }

    this.addEventListeners()
  }

  onMenuOverlayClick(event){
    event.preventDefault()
    this.closeDrawer()
    if(this.events.onDrawerClose) {
      this.events.onDrawerClose(this)
    }
  }

  onMenuButtonClick(event) {
    event.preventDefault()
    this.openDrawer()
    if(this.events.onDrawerOpen) {
      this.events.onDrawerOpen(this)
    }
  }

  isOpen() {
    if (this.menu) {
      return this.menu.classList.contains('show')
    }
    return false
  }

  openDrawer() {
    if (this.menu) {
      this.menu.classList.add('show')
    }

    DocumentElement.disableScroll()
  }

  closeDrawer() {
    if (this.menu) {
      this.menu.classList.remove('show')
    }

    DocumentElement.enableScroll()
  }



  onWindowTouchStart(event) {
    this._firstWindowTouch = event.changedTouches[0]
    this._hasTouchSwipeHandler = this._firstWindowTouch.target === this.swipeHandler;
    if(!this._hasTouchSwipeHandler) {
      return
    }

    this.menuOverlay.style.transition = 'none';
    this._containerRect = this.menuContent.getBoundingClientRect()
    this._setInitialTransform()
    this._removeDrawerTransition()
  }

  onWindowTouchMove(event) {
    if(!this._hasTouchSwipeHandler) {
      return
    }
    const touch = event.changedTouches[0]

    const opacity = this._getOpenOpacity(touch)
    const translateX = this._getOpenTranslateX(touch)
    
    this.menuOverlay.style.opacity = opacity;
    this._moveDrawer(translateX)
  }

  onWindowTouchCancel(event) {
    if(!this._hasTouchSwipeHandler) {
      return
    }
    this.menuOverlay.style.transition = '';
    this.menuOverlay.style.opacity = '';
    this._resetTouchElementStyles()
  }

  onWindowTouchEnd(event) {
    if(!this._hasTouchSwipeHandler) {
      return
    }
    const touch = event.changedTouches[0]
    const translateX = this._getOpenTranslateX(touch)

    this.menuOverlay.style.transition = '';
    this.menuOverlay.style.opacity = '';
    this._resetTouchElementStyles()
    /**
     * check in reverse because has negative translation.
     */
    if((this._containerRect.width - Math.abs(translateX)) > Math.abs(this._containerRect.width * this._threshold)) {
      this.openDrawer()
    } else {
      this.closeDrawer()
    }
  }

  _setInitialTransform() {
    let translateX = -this._containerRect.width
    if(this._direction === 'right') {
      translateX = this._containerRect.width
    }
    this.menuContent.style.transform = `translateX(${translateX}px)`;
  }

  _getOpenOpacity(touch) {
    let opacity = 0;

    if (this._direction === 'left') {
      opacity = map(
        touch.clientX,
        this._firstWindowTouch.clientX,
        window.innerWidth,
        0,
        1,
        true
      );
    } else {
      opacity = map(
        touch.clientX, // current touch X value
        this._firstWindowTouch.clientX, // start touch x value
        0, // end on the x = 0
        0, // opacity from 
        1, // opacity to
        true
      );
    }

    return opacity;
  }

  _getOpenTranslateX(touch) {
    const maxTranslate = window.innerWidth;
    let translateX = 0;

    if (this._direction === 'left') {
      translateX = map(
        touch.clientX,
        this._firstWindowTouch.clientX,
        maxTranslate,
        -this._containerRect.width,
        0,
        true
      );
    } else {
      translateX = map(
        touch.clientX,
        this._firstWindowTouch.clientX,
        0,
        this._containerRect.width,
        0,
        true
      );
    }

    return translateX;
  }



  // Touch Events
  // clientX: 411.4285583496094
  // clientY: 346.2184753417969
  // force: 0.38750001788139343
  // identifier: 0
  // pageX: 411.4285583496094
  // pageY: 346.2184753417969
  // radiusX: 20.259170532226562
  // radiusY: 20.259170532226562
  // rotationAngle: 0
  // screenX: 411.4285583496094
  // screenY: 426.2184753417969
  // target: div.js-mobile-main-menu-overlay.drawer__overlay

  onTouchStart(event) {
    console.log(event);
    this._cancelTouchIfNeeded(event)
    this._containerRect = this.menuContent.getBoundingClientRect()
    this._removeDrawerTransition()
    this._firstTouch = event.changedTouches[0]
  }

  onTouchMove(event) {
    if(this._cancelTouch) {
      return
    }
    const touch = event.changedTouches[0]
    if (this._isVerticalScroll(touch)) {
      this._resetTouchElementStyles();
      return
    }
    const translateX = this._getTranslateX(touch)
    this._moveDrawer(translateX)
  }

  onTouchCancel(event) {
    const touch = event.changedTouches[0]
    this._resetTouchElementStyles();
  }

  onTouchEnd(event) {
    if(this._cancelTouch) {
      return
    }
    const touch = event.changedTouches[0]
    if (this._isVerticalScroll(touch)) {
      this._resetTouchElementStyles()
      return
    }
    const translateX = this._getTranslateX(touch)
    this._resetTouchElementStyles()
    if (this._shouldTouchMoveCloseTheDrawer(translateX)) {
      this.closeDrawer()
    }
  }

  _cancelTouchIfNeeded(event) {
    if (!event) {
      return
    }
    if(!event.path) {
      return
    }
    const shouldCancelTouch = event.path.find(element => {
      let found = false;
      (this._cancelDragElements || []).forEach(className => {
        if (!element.classList) {
          return;
        }
        found = element.classList.contains(className)
      })
      return found;
    })
    this._cancelTouch = !!shouldCancelTouch
  }

  _isVerticalScroll(touch) {
    return Math.abs(touch.clientY - this._firstTouch.clientY) > Math.abs(touch.clientX - this._firstTouch.clientX)
  }

  _removeDrawerTransition() {
    this.menuContent.style.transition = 'none';    
  }

  _moveDrawer(translateX = 0) {
    this.menuContent.style.transform = `translateX(${translateX}px)`;
  }

  _shouldTouchMoveCloseTheDrawer(translateX = 0) {
    return Math.abs(translateX) > Math.abs(this._containerRect.width * this._threshold)
  }

  _getTranslateX(touch) {
    let maxTranslate = 0;
    let sign = -1;
    if(this._direction === 'right') {
      maxTranslate = window.innerWidth;
      sign = 1;
    }

    const translateX = map(
      touch.clientX,
      this._firstTouch.clientX,
      maxTranslate,
      0,
      this._containerRect.width,
      true
    );
    return sign * translateX
  }

  _resetTouchElementStyles() {
    this.menuContent.style.transition = '';
    this.menuContent.style.transform = '';
  }

}


function map(n, start1, stop1, start2, stop2, withinBounds) {
  const newval = (n - start1) / (stop1 - start1) * (stop2 - start2) + start2;
  if (!withinBounds) {
    return newval;
  }
  if (start2 < stop2) {
    return constrain(newval, start2, stop2);
  } else {
    return constrain(newval, stop2, start2);
  }
}

function constrain(n, low, high) {
  return Math.max(Math.min(n, high), low);
}


export default Drawer;