/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import AOS from 'aos';

import './bootstrap';

window.BRAKE_POINTS = {
  xs: 0,
  sm: 576,
  md: 768,
  lg: 1024,
  xl: 1336,
}

import { wp_ajax } from './wp'
import Sliders from './Sliders'
import { Translate } from './wp';
import MobileNavigation from './MobileNavigation'
import Drawer from './Drawer'



const sliders = window.SLIDERS = new Sliders;

(function () {
  AOS.init();

  document.querySelectorAll('.js-mobile-navigation').forEach(container => {
    new MobileNavigation(container);
  })

  new Drawer({
    containerSelector: '.js-mobile-main-menu',
    buttonSelector: '.js-toggle-mobile-menu',
    overlaySelector: '.js-mobile-main-menu-overlay',
  })
  sliders.add('.js-main-carousel', {
    setGallerySize: false,
    wrapAround: true,
  })

  wp_ajax('dummy_ajax2').get()
    .then(response => console.log(response))
    .catch(error => console.log(error))

  console.log(Translate.get('test'))



  // contact form placeholder

  function set_placeholder($class) {
    $($class).each(function (index, value) {
      var label = $(this).find('label').text();
      $(this).find('input').attr('placeholder', label);
      $(this).find('textarea').attr('placeholder', label);
      $(this).find('label').remove();
    });
  };
  set_placeholder('.vfb-item');


  if (document.getElementById('map')) {
    //Google Map
    function initMap() {
      const location = document.getElementById('map');
      if (!location) return;
      let coordinates = JSON.parse(location.dataset.marker);
      console.log(coordinates);
      coordinates = new google.maps.LatLng(coordinates.lat, coordinates.lng);

      const map = new google.maps.Map(location, {
        center: coordinates,
        zoom: 20,
        mapTypeControl: false,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
      });

      var marker = new google.maps.Marker({
        position: coordinates,
        map: map,
        //icon     : map_marker
      });
    }

    google.maps.event.addDomListener(window, 'load', initMap);
  }

  initSmoothScrolling()

})();


function initSmoothScrolling() {
  const smoothScrollLinks = document.querySelectorAll('.js-smooth-scroll')
  smoothScrollLinks.forEach(smoothScrollLink => {
    smoothScrollLink.addEventListener('click', (event) => {
      event.preventDefault();
      const selector = event.target.getAttribute('href');
      const el = document.querySelector(selector)
      if (!el) { return }
      el.scrollIntoView({
        behavior: 'smooth',
        block: 'start',
      });
    })
  })
}