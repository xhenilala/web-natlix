
import Flickity from 'flickity-as-nav-for'

class Sliders {
  constructor() {
    this._sliders = new Map();
  }

  add(selector, options) {
    const element = document.querySelector(selector)
    if (!element) {
      return null
    }
    this._sliders.set(
      selector,
      new Flickity(element, options)
    )

    return this._sliders.get(selector)
  }

  remove(selector) {
    if (!this._sliders.has(selector)) {
      return
    }
    const flickityInstance = this._sliders.get(selector)
    if (flickityInstance) {
      flickityInstance.destroy()
    }
    this._sliders.delete(selector)
  }
}

export default Sliders