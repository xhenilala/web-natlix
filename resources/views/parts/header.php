<div class="fixed-top">
    <nav class="navbar navbar-expand-lg navbar-light d-none d-lg-block py-1">
        <div class="container justify-content-between">
        <?php
            wp_nav_menu(array(
                'theme_location' => 'left-menu',
                'depth' => 2, // 1 = no dropdowns, 2 = with dropdowns.
                'container' => 'div',
                'container_class' => 'collapse navbar-collapse',
                'container_id' => 'left-menu',
                'menu_class' => 'navbar-nav',
                'fallback_cb' => 'App\Theme\NavWalker::fallback',
                'walker' => new App\Theme\NavWalker(),
            )); ?>
            <a class="navbar-brand" href="<?php echo esc_url(home_url('/')); ?>">
                <?php logo_img(); ?>
            </a>
            <?php
            wp_nav_menu(array(
                'theme_location' => 'right-menu',
                'depth' => 2, // 1 = no dropdowns, 2 = with dropdowns.
                'container' => 'div',
                'container_class' => 'collapse navbar-collapse',
                'container_id' => 'right-menu',
                'menu_class' => 'navbar-nav',
                'fallback_cb' => 'App\Theme\NavWalker::fallback',
                'walker' => new App\Theme\NavWalker(),
            )); ?>
        </div>
    </nav>
</div>
