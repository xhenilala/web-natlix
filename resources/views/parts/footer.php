<footer class="site-footer">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="footer-logo text-center">
                    <a href="<?php echo esc_url(home_url('/')); ?>">
                        <?php logo_img(); ?>
                    </a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-5">
                <div class="footer-menu">
                    <?php wp_nav_menu(array( 'theme_location' => 'left-menu', )); ?>
                </div>
            </div>
            <div class="col-2"></div>
            <div class="col-5">
                <div class="footer-menu">
                    <?php wp_nav_menu(array( 'theme_location' => 'right-menu', )); ?>
                </div>
            </div>
        </div>
        <div class="row my-3">
            <div class="col text-center">
                <div class="contact-information">
                    <?php $contact_info = get_field('contact_information','options'); echo apply_filters('the_content',$contact_info); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="social">
                    <?php view('general.social'); ?>
                </div>
            </div>
        </div>
    </div>
</footer><!-- #colophon -->
<?php // view('parts.photoswipe') ?>