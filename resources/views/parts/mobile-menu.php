<div class="mobile-header d-block d-sm-block d-md-block d-lg-none  py-1 fixed-top"> 
    <div class="container">
        <div class="row">
            <div class="col-12 mobile-header__items d-flex align-items-center ">
                <a class="js-toggle-mobile-menu" role="button" class="js-toggle-mobile-menu btn btn--flat" href="#" data-tab="#nav-shop-tab">    
                    <i class="fas fa-bars"></i>
                </a>
                <a class="site-logo m-auto" href="<?php echo esc_url(home_url('/')); ?>" >
                    <?php logo_img(); ?>
                </a>
            </div>
        </div>
    </div>
    <?php
    $menu_items = [
            [
                'label' => 'Shop',
                'id' => 'nav-shop-tab',
                'icon_class' => 'fas fa-shopping-bag',
                'href' => '#nav-shop',
                'active' => true,
                'content_id' => 'nav-shop',
                'key' => 'shop_tab',
                'content' => get_mobile_menu_html(),
            ],
        
        ];
    ?>
    <div class="js-mobile-main-menu drawer">
        <div class="js-mobile-main-menu-overlay drawer__overlay"></div>
        <div class="drawer__container">
            <div class="drawer__content tab-content" id="nav-tabContent">
                <?php foreach($menu_items as $m): $menu = collect($m); ?>
                <div
                    class="tab-pane fade <?php echo $menu->get('active', false) ? 'show active' : '' ?>"
                    id="<?php echo $menu->get('content_id'); ?>"
                    role="tabpanel"
                    aria-labelledby="<?php echo $menu->get('id'); ?>"
                >
                    <?php echo $menu->get('content') ?>
                </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>
