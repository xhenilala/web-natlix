<div class="section product-information">
    <div class="row">
    <?php
        $query = new WP_Query([
            'post_type' => 'information',
            'orderby'   => 'date',
            'order'     => 'ASC',
            'posts_per_page' => -1,
        ]);
        if( $query->have_posts() ) :  ?>
            <?php while( $query->have_posts() ) : $query->the_post(); ?>
                <div class="col-12 col-sm-12 col-md-6 col-lg-4 my-4 information">
                    <div class="information-item text-center" data-aos="fade-up" data-aos-delay="50" data-aos-duration="1000">
                        <div class="information-item__content"> 
                            <h3 class="my-3"><?php the_title(); ?></h3>
                            <p class="m-0"><?php echo get_the_content(); ?></p>
                        </div>
                    </div>
                </div>
            <?php endwhile; ?>
        <?php endif; ?>
    </div>
</div>