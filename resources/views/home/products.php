<div class="section home-products" id="products">
    <div class="container">
                <div class="row">   
                    <div class="col-12 col-lg-6">
                        <div class="home-products__entry">
                            <h1 data-aos="fade-right" data-aos-delay="50" data-aos-duration="1000"><?php echo get_field('products_title','options'); ?></h1>
                            <p data-aos="fade-right" data-aos-delay="50" data-aos-duration="1000"><?php echo get_field('products_content','options'); ?></p>
                            <a href="<?php echo get_field('products_button','options'); ?>" class="btn btn-primary"><?php echo __('Scopri i prodotti'); ?></a>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6 first-product">
                       <?php $featured_product = get_field('featured_product','options'); ?> 
                        <a href="<?php echo get_permalink($featured_product->ID); ?>" data-aos="fade-left" data-aos-delay="50" data-aos-duration="1000">
                            <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($featured_product->ID) ); ?>" alt="<?php echo $featured_product->post_title; ?>" class="img-fluid">
                        </a>
                    </div>
                </div>
        <div class="row mt-5 md-lg-5">
            <?php $products = collect(get_field('other_products','options')); ?>
            <?php foreach ( $products as $product ): $amazon_btn =  get_field('amazon_link', $product['product']->ID); ?>
                <div class="col-12 col-lg-3 mt-3  text-center natlix-item" data-aos="fade-up" data-aos-delay="50" data-aos-duration="1000" >
                    <!-- <a href="<?php //echo get_permalink($product['product']->ID); ?>"> -->
                        <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($product['product']->ID) ); ?>" class="img-fluid product-image" alt="<?php echo $product['product']->post_title;?>" class="img-fluid">
                        <div class="product-overlay">
                           
                               <div class="product-overlay__content">
                                <h6 class="my-2 text-lowercase"><?php echo get_the_title($product['product']->ID); ?></h6>
                                <a href="<?php echo get_permalink($product['product']->ID); ?>" class="btn btn-primary mb-3"><?php echo __('Scheda tecnica'); ?></a> <br>
                                <?php if($amazon_btn): ?>
                                    <a href="<?php echo $amazon_btn; ?>" class="btn btn-primary"><?php echo __('Acquista'); ?></a>
                                <?php endif; ?>
                               </div>
                            
                        </div>
                    <!-- </a> -->
                </div>
            <?php endforeach; ?>
        </div>
        <div class="row mt-5 md-lg-5">
            <?php $boundle = collect(get_field('boundle','options')); ?>
            <?php foreach ( $boundle as $item ): ?>
                <div class="col-12 col-lg-6 mt-3  text-center natlix-item" data-aos="fade-up" data-aos-delay="50" data-aos-duration="1000" >
                    <img src="<?php echo $item['boundle_image']; ?>" class="img-fluid product-image" alt="natlix boundle" class="img-fluid">
                    <div class="product-overlay">
                        <div class="product-overlay__content">
                            <a href="<?php echo $item['boundle_link']; ?>" class="btn btn-primary"><?php echo __('Acquista'); ?></a>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>