<?php
    $query = new WP_Query([
        'post_type' => 'service',
        'posts_per_page' => -1,
    ]);
    if( $query->have_posts() ) :  ?>
    <div class="section services">
        <div class="container">
            <div class="row">
                <?php while( $query->have_posts() ) : $query->the_post(); ?>
                    <div class="col-12 col-md-4 my-4 service">
                        <div class="service-item" data-aos="fade-up" data-aos-delay="50" data-aos-duration="1000">
                            <div class="service-item__image pr-4">
                                <img src="<?php echo the_post_img_url(); ?>" alt="<?php the_title(); ?>" class="img-fluid">
                            </div>
                            <div class="service-item__content">
                                <h5><?php the_title(); ?></h5>
                                <p class="m-0"><?php echo get_the_content(); ?></p>
                            </div>
                        </div>
                    </div>
                <?php endwhile; ?>
            </div>
        </div>
    </div>
    <?php endif; ?>