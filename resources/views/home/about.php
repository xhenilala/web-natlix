<div class="section home-about">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-6">
                <div class="home-about__content"> 
                    <h1 data-aos="fade-right" data-aos-delay="50" data-aos-duration="1000"><?php echo get_field('about_title','options'); ?></h1>
                    <p data-aos="fade-right" data-aos-delay="50" data-aos-duration="1000"><?php echo get_field('about_content','options'); ?></p>
                    <a href="<?php echo get_field('button','options'); ?>" class="btn btn-primary"><?php echo __('Scopri i nostri prodotti'); ?></a>
                </div>
            </div>
            <div class="col-12 col-lg-6">
                <div class="home-about__image" data-aos="fade-left" data-aos-delay="50" data-aos-duration="1000">
                    <img src="<?php echo get_field('about_image','options'); ?>" alt="about image" class="img-fluid">
                </div>
            </div>
        </div>
    </div>
</div>