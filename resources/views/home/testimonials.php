<div class="section testimonials">
    <div class="container"> 
        <div class="row">
            <div class="col">
                <h1 class="testimonials__title"><?php echo __('I commenti dei nostri clienti'); ?></h1>
            </div>
        </div>
        <div class="row">
        <?php  $query_args = [
            'post_type' => 'testimonial',
            'posts_per_page' => -1,
            ];
            $query = new WP_Query( $query_args );
            if( $query->have_posts() ) : ?>
                <div class="col"> 
                    <div class='testimonials-carousel' data-flickity='{"groupCells": true, "cellAlign": "center" , "autoPlay": false, "wrapAround": true}'>
                    <?php while( $query->have_posts() ) : $query->the_post(); $stars = get_field('stars'); ?>
                        <div class="cell">
                            <div class="testimonial-item">
                                <div class="testimonial-item__container">
                                    <img src="<?php echo the_post_img_url(); ?>" alt="avatar" class="img-fluid my-3">
                                    <h4><?php the_title(); ?></h4> 
                                    <?php for( $x=1; $x<=$stars; $x++) {
                                    echo '<i class="fa fa-star mb-3" aria-hidden="true"></i>'; }?>
                                    <?php the_content(); ?>
                                   
                                </div>
                            </div>
                        </div>
                    <?php endwhile; ?>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>