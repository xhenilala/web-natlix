<?php
$query = new WP_Query([
    'post_type' => 'slider',
    'posts_per_page' => -1,
]);
if( $query->have_posts() ) : ?>
<section class="section home-slider p-0">
    <div class="container-fluid">
        <div class="row">
            <div class="col p-0">
                <div class="js-main-carousel carousel main-carousel">
                    <?php while( $query->have_posts() ) : $query->the_post(); 
                        $buttons = get_field('buttons'); ?>
                        <div class="carousel-cell main-carousel__cell" style="background-image: url(<?php echo the_post_img_url(); ?>);" >
                            <div class="main-carousel__content px-3">
                                <h4 class="m-0" data-aos="fade-down" data-aos-delay="50" data-aos-duration="1000"><?php echo the_title(); ?></h4>
                                <p data-aos="fade-right" data-aos-delay="50" data-aos-duration="1000"><?php echo get_the_content(); ?></p>
                                <?php if( $buttons ): ?>
                                    <?php foreach( $buttons as $btn ): ?>
                                        <a href="<?php echo $btn['button_link']; ?>" class="js-smooth-scroll <?php echo $btn['button_class']; ?> mb-3" ><?php echo $btn['button_text']; ?></a><br>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </div>
                        </div> 
                    <?php endwhile;?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php endif; ?>