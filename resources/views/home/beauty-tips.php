<div class="section beauty-tip" id="beautytips">
    <div class="container">
        <div class="row">
            <div class="col">
                <h1 class="beauty-tip__title mt-0 mb-5"><?php echo get_field('beauty_information','options'); ?></h1>
            </div>
        </div>
        <div class="row">
        <?php
            $query = new WP_Query([
                'post_type' => 'beauty',
                'orderby'   => 'date',
                'order'     => 'ASC',
                'posts_per_page' => -1,
            ]);
            if( $query->have_posts() ) :  ?>
                <?php while( $query->have_posts() ) : $query->the_post(); ?>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-3 my-4 tip">
                        <div class="tip-item text-center" data-aos="fade-up" data-aos-delay="50" data-aos-duration="1000">
                            <div class="tip-item__image mb-3">
                                <img src="<?php echo the_post_img_url(); ?>" alt="<?php the_title(); ?>" class="img-fluid">
                            </div>
                            <div class="tip-item__content"> 
                                <p class="m-0"><?php echo get_the_content(); ?></p>
                            </div>
                        </div>
                    </div>
                <?php endwhile; ?>
            <?php endif; ?>
        </div>
    </div>
</div>