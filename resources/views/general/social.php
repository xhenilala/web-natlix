
<?php $socials = collect(get_field('social','options')); ?>
<?php foreach( $socials as $social ): ?>
        <a href="<?php echo $social['social_link']; ?>" target="blank">
            <i class="<?php echo $social['social_icon']; ?>"></i>
        </a>
<?php endforeach; ?>