<div class="section contact">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="contant-us">
                    <div class="contact-us__title" data-aos="fade-right" data-aos-delay="50" data-aos-duration="1000">
                        <h1><?php echo get_field('contact_title','options'); ?></h1>
                    </div>
                    <div class="contact-us__content mb-5" data-aos="fade-left" data-aos-delay="50" data-aos-duration="1000">
                        <?php echo get_field('contact_content','options'); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="contact__content" data-aos="fade-up" data-aos-delay="50" data-aos-duration="1000">
                    <?php echo do_shortcode("[vfb id=1]"); ?>
                </div>
            </div>
        </div>
    </div>
</div>