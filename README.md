# - Backend 

Custom filters
* `nmc_js_initial_data` args: $data
* `nmc_register_sliders` args: $sliders_config

Helpers
* `view` ex:  `view('header')` where header is in `resources/views` folder
* `logo` and `logo_img`
* `config` get config values from config folder

## Create a Post Type

1. create new file in  `app/Database/PostTypes` folder.  
    Example: `Slider.php`.  
    Note: File must be named in CamelCase Style.

2. Create class with same name as file. Example `Slider`    

```php
<?php

namespace App\Database\PostTypes;

use App\Base\Database\PostType;

class Slider {
    public function create() {
        return PostType::create()
            ->slug('slider')
            ->name(__('Slider', 'nmc'))
            ->menu_icon('dashicons-slides')
            ->register();
    }
}
```
3. Done!

---

## Create a Taxonomy

1. create new file in  `app/Database/Taxonomies` folder.   
    Example: `ProductCategory.php`.     
    Note: File must be named in CamelCase Style.

2. Create class with same name as file. Example `ProductCategory`   

```php
<?php

namespace App\Database\Taxonomies;

use App\Base\Database\Taxonomy;

class ProductCategory {
    public function create() {
        return Taxonomy::create()
            ->set_object_type(['product'])
            ->slug('product_category')
            ->name(__('Product Category', 'nmc'))
            ->register();
    }
}
```
3. Done!

---

## Create Hooks ( *filters or actions* )

1. create new file in  `app/Hooks` folder.   
    Example: `BodyHook.php`.     
    Note: File must be named in CamelCase Style.

2. Create class with same name as file. Example `BodyHook`  

```php
<?php

namespace App\Hooks;

use App\Base\Singleton;


class BodyHook extends Singleton
{
    protected function __construct()
    {
        add_filter( 'body_class', [$this, 'body_classes'] );
    }

    /**
    * Adds custom classes to the array of body classes.
    *
    * @param array $classes Classes for the body element.
    * @return array
    */
    public function body_classes($classes)
    {
        return $classes;
    }
}

```
3. Load Hook in `Init` class in `app` folder    

```php
<?php

namespace App;

use App\Base\Singleton;
// more code here

use App\Hooks\BodyHook;


class Init extends Singleton
{

    protected function __construct()
    {
        // more code here
        BodyHook::init();
    }
}
```
4. Done!

---

## Create General Classes

1. create new file in  `app` folder.
    Example: `Site.php`.     
    Note: File must be named in CamelCase Style.

2. Create class with same name as file. Example `Site`  

```php
<?php

namespace App;

class Site
{
    public static function logo() {
        $custom_logo_id = get_theme_mod( 'custom_logo' );
        $image = wp_get_attachment_image_src( $custom_logo_id , 'full' );

        if (!$image) {
            return false;
        }

        return [
            'url' => $image[0],
            'width' => $image[1],
            'height' => $image[2],
            'is_intermediate' => $image[3],
        ];
    }

    public static function logo_img() {
        $logo = static::logo();
        ?>
        <?php if ($logo) : ?>
            <img
                src="<?php echo $logo['url'] ?>"
                width="<?php echo $logo['width'] ?>"
                height="<?php echo $logo['height'] ?>"
                alt="<?php bloginfo('name'); ?>" />
        <?php else :
            bloginfo('name');
        endif; ?>
        <?php
    }

}
```
3. (*optional*) Initialize in `Init` class if you need
4. Done!

---

## Config Your Site

1. open `config/app.php`
2. change for you needs 

```php
<?php
// default config file
return [
    "supports" => [
        "title-tag" => null,
        "post-thumbnails" => null,
        "post-formats" => ['gallery', 'video'],
        "custom-header" => null,
        "custom-logo" => null,
        "html5" => [
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption'
        ],
    ],

    "admin_option_page" => [
        'page_title' => __('Options', 'nmc'),
        'menu_title' => __('Options', 'nmc'),
        'menu_slug' => 'options',
        'capability' => 'edit_posts',
        'redirect' => false,
        'icon_url' => 'dashicons-admin-generic',
    ],

    'nav_menus' => [
        'primary' => __('Primary Menu', 'nmc'),
    ],
    "sidebars" => [
        "sidebar" => [
            'name' => __('Sidebar', 'nmc'),
            'description' => __('Default sidebar', 'nmc'),
        ],
    ]
];

```

### - Add more config files
    * create new file in `config` folder
```php
// site.php
<?php
    return [
        'name' => 'NMC',
    ];
```
* use if with `config` helper
```php
    <?php
        $name = config('site.name', 'Default Value');
        dd($name);
```


# - Frontend

## Install/Compile your assets
```console
dev-nmc:nmc-theme xheni$ cd /path/to/theme
dev-nmc:nmc-theme xheni$ npm install
dev-nmc:nmc-theme xheni$ npm install --save-dev <package> to install new package
dev-nmc:nmc-theme xheni$ npm run dev // use when developing
dev-nmc:nmc-theme xheni$ npm run prod // use when deploying to production
```
```
// all commands
npm run dev // alias of development
npm run development
npm run watch
npm run watch-poll
npm run hot
npm run prod // alias of production
npm run production
```


# - CONSOLE
```console
dev-nmc:nmc-theme xheni$ php please
Usage:
  command [options] [arguments]

Options:
  -h, --help            Display this help message
  -q, --quiet           Do not output any message
  -V, --version         Display this application version
      --ansi            Force ANSI output
      --no-ansi         Disable ANSI output
  -n, --no-interaction  Do not ask any interactive question
  -v|vv|vvv, --verbose  Increase the verbosity of messages: 1 for normal output, 2 for more verbose output and 3 for debug

Available commands:
  help               Displays help for a command
  list               Lists commands
 theme
  theme:text-domain  Change theme text domain

```


