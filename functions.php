<?php
/**
 * NMC Theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package NMC_Theme
 */

require get_template_directory() . '/vendor/autoload.php';

App\Init::init();
