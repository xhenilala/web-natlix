<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package NMC_Theme
 */

get_header(); ?>

    <?php view('home.slider'); ?>

    <?php view('home.beauty'); ?>

    <?php view('home.beauty-tips'); ?>

    <?php view('home.about'); ?>

    <?php view('home.services'); ?>

    <?php view('home.products'); ?>

    <?php view('home.testimonials'); ?>

    <?php view('general.contact'); ?>

<?php get_footer();
