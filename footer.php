<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package NMC_Theme
 */

?>

	</div><!-- #content -->
	<?php view("parts.footer") ?>
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
