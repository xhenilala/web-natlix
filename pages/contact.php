<?php
    /* Template Name: Contact*/
    get_header();
?>
<div class="container">
    <div class="row">
        <div class="col p-0">
            <?php view('general.contact'); ?>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row" >
        <div class="col p-0">
            <?php $map = get_field('map_iframe', 'options'); if($map): ?>
                <?php echo $map; ?>
            <?php endif; ?>
        </div>
    </div>
</div>

<?php get_footer(); ?>