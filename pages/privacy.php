<?php
    /* Template Name: Privacy*/
    get_header();
?>
<div class="container">
    <div class="privacy pb-5">
        <div class="row">
            <?php while(have_posts()): the_post(); ?>
                <div class="col">
                    <div class="privacy__title" data-aos="fade-right" data-aos-delay="50" data-aos-duration="1000">
                        <h1><?php the_title(); ?></h1>
                    </div>
                    <div class="privacy__content" data-aos="fade-left" data-aos-delay="50" data-aos-duration="1000">
                        <?php the_content(); ?>
                    </div>
                </div>
            <?php endwhile; ?>
        </div>
    </div>
    
</div>
<?php get_footer(); ?>