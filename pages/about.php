<?php
    /* Template Name: About*/
    get_header();
?>
<div class="about">

        <?php while(have_posts()): the_post(); ?> 
            <div class="about__cover" style="background-image: url(<?php the_post_img_url(); ?>);">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <div class="about__content">
                                <div class="about__inner-content" data-aos="fade-down" data-aos-delay="50" data-aos-duration="1000">
                                    <?php the_content(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- <div class="about__overlay"></div> -->
            </div> 
        <?php endwhile; ?>

    <div class="container">
        <div class="row inner-section">
            <div class="col">
                <div class="about__entry" data-aos="fade-right" data-aos-delay="50" data-aos-duration="1000">
                    <h2 class="mt-3 mb-5"><?php echo get_field('entry_title'); ?></h2>
                    <h4 class="mb-3"><?php echo get_field('answer_one_title'); ?></h4>
                    <p><?php echo get_field('answer_one_content'); ?></p>
                </div>
            </div>
        </div> 
        <div class="row mb-5">
            <div class="col-12 col-lg-6">
                <div class="about__entry" data-aos="fade-right" data-aos-delay="50" data-aos-duration="1000">
                    <img src="<?php echo get_field('answer_two_image'); ?>" alt="<?php the_title(); ?>" class="img-fluid">
                </div>
            </div>
            <div class="col-12 col-lg-6">
            <div class="about__entry" data-aos="fade-right" data-aos-delay="50" data-aos-duration="1000"> 
                <h4 class="mb-3 mt-3 mt-md-0"><?php echo get_field('answer_two_title'); ?></h4>
                <p><?php echo get_field('answer_two_content'); ?></p>
            </div>
            </div>
        </div> 
    </div>
    <div class="container">
        <?php view('about.product-information'); ?>
    </div>
    <div class="container">
        <div class="row">
            <div class="col mb-5 text-center">
                <a href="./contact" class="btn btn-primary"><?php echo __('Contattaci'); ?></a>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>