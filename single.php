<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package NMC_Theme
 */

get_header();  ?>
    <div class="single-item">
        <?php while(have_posts() ): the_post(); 
        $img_src = wp_get_attachment_url( get_post_thumbnail_id(), 'full', false );
        $amazon_btn = get_field('amazon_link'); $gallery = collect(get_field('gallery')); 
        $ingredients = collect(get_field('ingredients'));
        $usage = get_field('usage'); ?>
        <div class="single-item-header">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 single-item__details">
                    <div class="item-arrow-left d-none d-lg-block">
                    <?php if( get_adjacent_post(false, '', true) ) : ?>
                    <?php previous_post_link('%link', '&#x3c;', FALSE); ?>
                    <?php else : ?>
                        <?php $first = new WP_Query('post_type=product&posts_per_page=1&order=DESC'); $first->the_post(); ?>
                        <?php echo '<a href="' . get_permalink() . '">&#x3c</a>'; wp_reset_query(); ?>
                    <?php endif; ?>
                </div>
                <p><?php echo get_field('sku'); ?></p>
                <div class="single-item__content" data-aos="fade-right" data-aos-delay="50" data-aos-duration="1000">
                    <h1><?php the_title(); ?></h1>
                    <p><?php the_content(); ?></p>
                </div>
                <?php if( $amazon_btn ): ?>
                    <a href="<?php echo $amazon_btn; ?>" class="btn btn-primary btn-primary--amazon" target="_blank"><?php echo __('Acquista');?></a>
                <?php endif;?>
                <div class="d-block d-lg-none d-flex mt-4 item-arrows">
                <?php if( get_adjacent_post(false, '', true) ) : ?>
                <?php previous_post_link('%link', '&#x3c;', FALSE); ?>
                <?php else : ?>
                    <?php $first = new WP_Query('post_type=product&posts_per_page=1&order=DESC'); $first->the_post(); ?>
                    <?php echo '<a href="' . get_permalink() . '">&#x3c</a>'; wp_reset_query(); ?>
                <?php endif; ?>
                <?php if( get_adjacent_post(false, '', false) )  : ?>
                    <?php next_post_link('%link', '&#x3e;', FALSE); ?>
                <?php else: ?>
                <?php $last = new WP_Query('post_type=product&posts_per_page=1&order=ASC'); $last->the_post(); ?>
                    <?php echo '<a href="' . get_permalink() . '">&#x3e;</a>'; wp_reset_query(); ?>
                <?php endif; ?>
                </div>
                </div>
                <div class="col-12 col-sm-12 col-md-12 col-lg-6 single-item__gallery" data-aos="fade-left" data-aos-delay="50" data-aos-duration="1000">
                    <div class="carousel product-gallery" data-flickity='{ "autoPlay": true }'>
                        <?php foreach( $gallery as $img ):   ?>
                            <div class="carousel-cell product-gallery__cell" >
                                <img src="<?php echo $img['sizes']['medium']; ?>" alt="<?php the_title(); ?>" class="img-fluid">
                            </div>
                        <?php endforeach; ?>
                    </div>   
                </div>
                <div class="item-arrow-right  d-none d-lg-block"> 
                    <?php if( get_adjacent_post(false, '', false) )  : ?>
                        <?php next_post_link('%link', '&#x3e;', FALSE); ?>
                    <?php else: ?>
                    <?php $last = new WP_Query('post_type=product&posts_per_page=1&order=ASC'); $last->the_post(); ?>
                        <?php echo '<a href="' . get_permalink() . '">&#x3e;</a>'; wp_reset_query(); ?>
                    <?php endif; ?>
    
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-4 single-item__information" data-aos="fade-up" data-aos-delay="50" data-aos-duration="1000">
                <h3><?php echo __('Ingredienti:'); ?></h3>
                <ul>
                    <?php foreach( $ingredients as $ingredient ): ?>
                        <li><?php echo $ingredient['ingredient']; ?></li>
                    <?php endforeach; ?>
                </ul>
            </div>
            <div class="col-12 col-md-8 single-item__information" data-aos="fade-up" data-aos-delay="50" data-aos-duration="1000">
                <h3><?php echo __('Modo d’uso:'); ?></h3>
                <?php echo $usage; ?>
            </div>
        </div>
    </div>
        <?php endwhile; ?>
    </div>
   
   <div class="container">
   <div class="row">
        <div class="col">
            <h1 class="realted-products-title"><?php echo __('Gli altri prodotti della gamma Natlix'); ?></h1>
        </div>
   </div>
    <div class="row related-products">
        <?php $queried_object = get_queried_object_id();  ?>
            <?php
                $query = new WP_Query([
                'post_type' => 'product',
                'posts_per_page' => -1,
                'post__not_in' => array($queried_object), 
                ]);
            if( $query->have_posts() ) : ?>
                <?php while( $query->have_posts() ) : $query->the_post(); $amazon_btn = get_field('amazon_link'); ?>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-3 text-center mb-3 natlix-item" data-aos="fade-up" data-aos-delay="50" data-aos-duration="1000">
                        <img src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>" class="img-fluid product-image">
                        <div class="product-overlay">
                            <div class="product-overlay__content">
                            <h6 class="my-2 text-lowercase"><?php the_title(); ?></h6>
                            <a href="<?php echo the_permalink(); ?>" class="btn btn-primary mb-3"><?php echo __('Scheda tecnica'); ?></a> <br>
                            <?php if($amazon_btn): ?>
                                <a href="<?php echo $amazon_btn; ?>" class="btn btn-primary"><?php echo __('Acquista'); ?></a>
                            <?php endif; ?>
                            </div>
                        </div> 
                    </div>
                <?php endwhile; ?>
            <?php endif; ?>
        </div>
   </div>
<?php get_footer(); ?> 
