<?php

namespace App;

use App\Base\Support\Str;

class Site
{
    public static function svg_logo($attributes = [], \Closure $callback = null) {
        $custom_logo_id = get_theme_mod( 'custom_logo' );
        $logo_path = get_attached_file($custom_logo_id);

        if (Str::endsWith($logo_path, '.svg')) {
            return get_svg($logo_path, $attributes, $callback);
        }

        return static::logo_img();
    }

    public static function logo() {
        $custom_logo_id = get_theme_mod( 'custom_logo' );
        $image = wp_get_attachment_image_src( $custom_logo_id , 'full' );
        if (!has_custom_logo()) {
            return false;
        }

        return [
            'url' => $image[0],
            'width' => $image[1],
            'height' => $image[2],
            'is_intermediate' => $image[3],
        ];
    }

    public static function logo_img() {
        $logo = static::logo();
        ?>
        <?php if ($logo) : ?>
            <img src="<?php echo $logo['url'] ?>" width="<?php echo $logo['width'] ?>"  height="<?php echo $logo['height'] ?>" alt="<?php bloginfo('name'); ?>">
        <?php else :
            bloginfo('name');
        endif; ?>
        <?php
    }

}
