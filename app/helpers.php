<?php

/**
 * 
 * 
 * Enter your helpers here
 * 
 * 
 */

if (!function_exists('logo')) {
    function logo()
    {
        return App\Site::logo();
    }
}

if (!function_exists('logo_url')) {
    function logo_url()
    {
        $logo = App\Site::logo();
        return $logo ? $logo['url'] : '';
    }
}

if (!function_exists('logo_img')) {
    function logo_img()
    {
        return App\Site::logo_img();
    }
}

if (!function_exists('svg_logo')) {
    function svg_logo($attributes = [], \Closure $callback = null)
    {
        echo App\Site::svg_logo($attributes, $callback);
    }
}
if (!function_exists('the_post_img_url')) {
    /**
     * Echo post img url
     */
    function the_post_img_url()
    {
        echo wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()));
    }
}
if (!function_exists('is_current_term')) {
    /**
     * Helper function to check if term is current
     * @param array $term
     * @param int $current_term
     * @return bool
     */
    function is_current_term($term, $current_term)
    {
        if ($term['term_id'] == $current_term) {
            return true;
        }

        $next_terms = $term['children'];
        if (!is_array($next_terms)) {
            return false; // the term is not current and has no children
        }

        $current = array_reduce($next_terms, function ($current, $next_term) use ($current_term) {
            return is_current_term($next_term, $current_term) ? true : $current;
        }, false);
        return $current;
    }
}


if (!function_exists('is_category_selected')) {
    /**
     * Helper function to check if category is selected
     * @param array $level
     * @return bool
     */
    function is_category_selected($level)
    {
        $queried_object = get_queried_object();

        if (!is_a($queried_object, 'WP_Term')) {
            return false;
        }

        return is_current_term($level, $queried_object->term_id);
    }
}


function get_mobile_menu_html() {
    ob_start();
    render_mobile_menu();
    $menu_tree_html = ob_get_contents();
    ob_clean();
    return $menu_tree_html;
}

function render_mobile_menu() {
    $main_menu_tree = \App\MainMenu::init()->get_tree();
    ?>
        <div class="js-mobile-navigation mobile-navigation">
            <div class="mobile-navigation__container">
                <div class="mobile-navigation__header text-center pt-4 pb-2">
                    <a class="site-logo ml-auto" href="<?php echo esc_url(home_url('/')); ?>" >
                        <?php logo_img(); ?>
                    </a>
                </div>
                <div class="mobile-navigation__body my-4">
                    <?php wp_nav_menu(array( 'theme_location' => 'primary' )); ?>
                </div>
                <div class="mobile-navigation__footer">
                    <?php view('general.social'); ?>
                </div>
            </div>
        </div>
    <?php
}


/**
 *  Infinite next and previous post looping in WordPress
 */

// if( is_singular( [ 'product' ] ) ){
   
//     if( get_adjacent_post(false, '', true) ) { 
//         previous_post_link('%link', '&larr; Previous Post');
//     } else { 
//         $first = new WP_Query('post_type=product&posts_per_page=1&order=DESC'); $first->the_post();
//             echo '<a href="' . get_permalink() . '">&larr; Previous Post</a>';
//         wp_reset_query();
//     } 

//     if( get_adjacent_post(false, '', false) ) { 
//         next_post_link('%link', 'Next Post &rarr;');
//     } else { 
//         $last = new WP_Query('post_type=productposts_per_page=1&order=ASC'); $last->the_post();
//             echo '<a href="' . get_permalink() . '">Next Post &rarr;</a>';
//         wp_reset_query();
//     }
// }