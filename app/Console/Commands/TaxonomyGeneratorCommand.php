<?php

namespace App\Console\Commands;

use App\Base\Support\Str;
use App\Base\Support\Arr;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TaxonomyGeneratorCommand extends GeneratorCommand
{
    /**
     * @var string
     */
    protected $name = 'Taxonomy';

    /**
     * @var array
     */
    protected $posts = [];

    protected function configure()
    {
        $this
        ->setName('make:taxonomy')
        ->setDescription('Create new taxonomy')
        ->setHelp("This command allows you to create new taxonomy;\nArguments: {name};\nOptions: {--posts}")
        ->setDefinition(
            new InputDefinition(array(
                new InputArgument('name', InputArgument::REQUIRED),
                new InputOption('--posts', null, InputOption::VALUE_REQUIRED | InputOption::VALUE_IS_ARRAY)
            ))
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $posttype = $input->getOption('posts');
        $this->setPosts($posttype);
        parent::execute($input, $output);
    }

    /**
     * @param array $posts 
     */
    protected function setPosts(array $posts)
    {
        $newPosts = array_map(function ($post) {
            return array_map(function($item) { return trim($item); }, explode(',', $post));
        }, $posts);

        $this->posts = Arr::flatten($newPosts);
    }

    /**
     * Get the default namespace for the class.
     *
     * @param  string  $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace.'\Database\Taxonomies';
    }

    /**
     * Replace the class name for the given stub.
     *
     * @param  string  $stub
     * @param  string  $name
     * @return string
     */
    protected function replaceClass($stub, $name)
    {
        $class = str_replace($this->getNamespace($name).'\\', '', $name);

        $name = ucwords(str_replace(['-', '_'], ' ', Str::kebab($class)));

        $postsWithQuotes = array_map(function($item) { return "'".$item."'"; }, $this->posts);
        $posts = '['.implode(',', $postsWithQuotes).']';

        $content = str_replace(
            ['DummyClass', 'dummy-class', 'Dummy Class', 'DummyPostTypes', 'textdomain'],
            [$class, Str::kebab($class), $name, $posts, getTextDomain()],
            $stub
        );

        return $content; //str_replace('DummyClass', $class, $stub);
    }

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        return __DIR__.'/stubs/taxonomy.stub';
    }

}