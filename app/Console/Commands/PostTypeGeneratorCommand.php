<?php

namespace App\Console\Commands;

use App\Base\Support\Str;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputArgument;

class PostTypeGeneratorCommand extends GeneratorCommand
{
    /**
     * @var string
     */
    protected $name = 'Post type';

    protected function configure()
    {
        $this
        ->setName('make:post-type')
        ->setDescription('Create new post type')
        ->setHelp("This command allows you to create new post type;\nArguments: {name}")
        ->setDefinition(
            new InputDefinition(array(
                new InputArgument('name', InputArgument::REQUIRED),
            ))
        );
    }

    /**
     * Get the default namespace for the class.
     *
     * @param  string  $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace.'\Database\PostTypes';
    }

    /**
     * Replace the class name for the given stub.
     *
     * @param  string  $stub
     * @param  string  $name
     * @return string
     */
    protected function replaceClass($stub, $name)
    {
        $class = str_replace($this->getNamespace($name).'\\', '', $name);

        $name = ucwords(str_replace(['-', '_'], ' ', Str::kebab($class)));
        $content = str_replace(
            ['DummyClass', 'dummy-class', 'Dummy Class', 'textdomain'],
            [$class, Str::kebab($class), $name, getTextDomain()],
            $stub
        );

        return $content; //str_replace('DummyClass', $class, $stub);
    }

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        return __DIR__.'/stubs/post-type.stub';
    }

}