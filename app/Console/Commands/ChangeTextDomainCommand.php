<?php

namespace App\Console\Commands;

use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;

class ChangeTextDomainCommand extends Command
{
    /**
     * @var \Symfony\Component\Finder\Finder
     */
    protected $finder;

    /**
     * @var string
     */
    protected $baseDir;

    /**
     * @var string
     */
    protected $pattern;

    /**
     * @var string
     */
    protected $replacement;

    public function __construct(string $baseDir)
    {
        $this->baseDir = $baseDir;
        $this->finder = Finder::create();

        parent::__construct();
    }

    protected function configure()
    {
        $this
        ->setName('theme:text-domain')
        ->setDescription('Change theme text domain')
        ->setHelp("This command allows you to change theme textdomain;\nArguments: {new-textdomain}")
        ->setDefinition(
            new InputDefinition(array(
                // new InputArgument('textdomain', InputArgument::REQUIRED),
                new InputArgument('new-textdomain', InputArgument::REQUIRED),
            ))
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $old_textdomain = getTextDomain(); //$input->getArgument('textdomain');
        $new_textdomain = $input->getArgument('new-textdomain');

        $this->pattern = "/(__\(.*?['\"])({$old_textdomain})(['\"].*?\))/";
        $this->replacement = "\${1}{$new_textdomain}\${3}";

        $this->replaceTextDomainInAllPhpFiles();
        $this->changeTextDomainInStyle($old_textdomain, $new_textdomain);

        $output->writeln('<info>TEXT DOMAIN was changed successfully!</info>');
    }

     /**
     *
     * @return void
     */
    protected function replaceTextDomainInAllPhpFiles() {
        $files = $this->finder->in($this->baseDir)
                    ->contains($this->pattern)
                    ->exclude([
                        'app/Console',
                        'app/Base',
                        'resources/assets',
                        'vendor',
                        'none_modules',
                        'logs',
                        'build',
                    ])
                    ->name('*.php');

        $dbFiles = $this->finder->in($this->baseDir . '/app/Base/Database')
                    ->contains($this->pattern)
                    ->name('*.php');
        
        foreach ($files as $file) {
            $this->replaceTextDomain($file);
        }

        foreach ($dbFiles as $file) {
            $this->replaceTextDomain($file);
        }
    }

    /**
     *
     * @param  \Symfony\Component\Finder\SplFileInfo $file
     * @return void
     */
    protected function replaceTextDomain(SplFileInfo $file)
    {
        $content = $file->getContents();
        $newContent = preg_replace($this->pattern, $this->replacement, $content);
        file_put_contents($file->getRealPath(), $newContent);
    }

    /**
     *
     * @param string $old_textdomain
     * @param string $new_textdomain
     * @return void
     */
    protected function changeTextDomainInStyle($old_textdomain, $new_textdomain)
    {
        $file = $this->baseDir.'/style.css';
        $search = 'Text Domain: '.$old_textdomain;
        $replace = 'Text Domain: '.$new_textdomain;

        $content = file_get_contents($file);

        $newContent = str_replace($search, $replace, $content);
        
        file_put_contents($file, $newContent);
    }
}