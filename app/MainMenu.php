<?php

/**
 * class to get main site menu tree
 */
namespace App;

use App\Base\Singleton;

/**
 * MainMenu class
 */
class MainMenu extends Singleton
{

    /**
     * Register hooks
     */
    public function __construct()
    {
        add_filter(app_prefix() . '_category', [$this, 'category_filter'], 20, 1);
    }

    /**
     * Get menu category tree
     *
     * @return array
     */
    public function get_tree()
    {
        // return $tree = $this->build_tree();
        // delete_transient( app_prefix() . '_main_menu_tree_list_' );

        if (false === ($tree = get_transient(app_prefix() . '_main_menu_tree_list_'))) {
            $tree = $this->build_tree();
            set_transient(app_prefix() . '_main_menu_tree_list_', $tree, 1 * MONTH_IN_SECONDS);
        }

        return $tree;
    }

    /**
     * Get product categories
     * @param  integer $category catgory id
     * @param int $level
     * @return array
     */
    public function build_tree($category = 0, $level = 0)
    {

        $tree = [];
        $args = [
            'taxonomy' => 'category',
            'parent' => $category,
            'hide_empty' => 0,
            'orderby' => 'date',
            'order' => 'DESC',
            'meta_query' => [
                'relation' => 'OR',
                [
                    'key' => 'category_visibility',
                    'value' => '',
                    'compare' => 'NOT EXISTS',
                ],
                [
                    'key' => 'category_visibility',
                    'value' => '0',
                    'compare' => '=',
                ],
            ],
        ];

        $next = get_terms('category', $args);
        if ($next) {
            foreach ($next as $term) {
                $term_array = (array)$term;
                $term_array['level'] = $level;
                $tree[$term->term_id] = apply_filters(app_prefix() . '_category', $term_array);
                $tree[$term->term_id]['children'] = $term->term_id !== 0 ?
                    $this->build_tree($term->term_id, $level + 1) : null;
            }
            return $tree;
        }

        return null;
    }

    /**
     * Get featured artcile based on product category id
     * @param  integer $term_id
     * @return interger
     */
    public function get_term_featured_article($term_id)
    {

        $post_args = [
            'post_type' => 'post',
            'posts_per_page' => 1,
            'post_status' => 'publish',
            'tax_query' => [[
                'taxonomy' => 'category',
                'field' => 'term_id',
                'terms' => $term_id
            ]],
            
        ];


        $posts = get_posts($post_args);

        if (count($posts)) {
            return $posts[0]->ID;
        }
    }

    /**
     * Add or remove category metadata
     * @param  array $term WP_Term as array
     * @return array
     */
    public function category_filter($term)
    {

        // $queried_object = get_queried_object();
        // $current_term = 0;

        // if (is_a($queried_object, 'WP_Term')) {
        //     $current_term = $queried_object->term_id;
        // }

        if (is_array($term) && array_key_exists('term_id', $term)) {
            // $term['is_active'] = false;
            $term['link'] = get_term_link($term['term_id']);
            $term['featured_article'] = $this->get_term_featured_article($term['term_id']);

            // if ($term['parent'] == 0 && $term['term_id'] == $current_term) {
            //     $term['is_active'] = true;
            // }
        }

        return $term;
    }
}
