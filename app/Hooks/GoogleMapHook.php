<?php

namespace App\Hooks;

use App\Base\Singleton;


class GoogleMapHook extends Singleton
{
    protected function __construct()
    {
        add_filter('acf/fields/google_map/api', [ $this, 'my_acf_google_map_api'] );
    }

    public function my_acf_google_map_api( $api ){
	
        $api['key'] = 'AIzaSyB42ga4NVJW73TO7Jd05o7kyldh_eL1ko4';
        
        return $api;
        
    }
}
