<?php

namespace App\Base;

abstract class Singleton
{
    private static $instances = [];
    protected function __construct() {}
    public static function init()
    {
        if (!isset(self::$instances[static::class])) {
            self::$instances[static::class] = new static();
        }
        // else {
        //     dump_caller();
        //     trigger_error(static::class . " instance exists. You shouldn't create two instances in singleton class!", E_USER_NOTICE);
        // }
        // dump(self::$instances);
        return self::$instances[static::class];
    }

    public static function getInstance() {
        return static::init();
    }

    private function __clone() {}
    private function __wakeup() {}
} 