<?php

namespace App\Base\Bootstrap;

use App\Base\Singleton;
use Symfony\Component\Finder\Finder;

class RegisterAjaxHooks extends Singleton
{
    public $namespace = '\App\Http';
    public $directory = 'app/Http';

    public function __construct()
    {
        $this->register_all();
    }

    public function register_all()
    {
        foreach ($this->find_all() as $key => $class) {
            $this->register($class);
        }
    }

    protected function register($class)
    {
        if (class_exists($class)) {
            $class::init();                
        }
    }

    protected function find_all()
    {
        $path = TEMPLATEPATH.DIRECTORY_SEPARATOR.$this->directory;
        $files = [];

        foreach (Finder::create()->files()->name('*.php')->in($path) as $file) {
            $fileName = basename($file->getRealPath(), '.php');
            $files[$fileName] = $this->namespace . '\\' . $fileName;
        }

        ksort($files, SORT_NATURAL);

        return $files;
    }

}
