<?php

namespace App\Base\Bootstrap;

use App\Base\Singleton;
use Symfony\Component\Finder\Finder;

class RegisterTaxonomies extends Singleton
{
    public $namespace = '\App\Database\Taxonomies';
    public $directory = 'app/Database/Taxonomies';

    public function __construct()
    {
        $this->register_all();
    }

    public function register_all()
    {
        foreach ($this->find_all() as $key => $class) {
            $this->register($class);
        }
    }

    protected function register($class)
    {
        $instance = (new $class())->create();
        if ($instance && (new \ReflectionClass($instance))->getShortName() === 'RegisterTaxonomy') {
            add_action('init', [$instance, 'register'], 20);
        }
    }

    protected function find_all()
    {
        $path = TEMPLATEPATH.DIRECTORY_SEPARATOR.$this->directory;
        $files = [];

        foreach (Finder::create()->files()->name('*.php')->in($path) as $file) {
            $fileName = basename($file->getRealPath(), '.php');
            $files[$fileName] = $this->namespace . '\\' . $fileName;
        }

        ksort($files, SORT_NATURAL);

        return $files;
    }

}
