<?php

namespace App\Database\PostTypes;

use App\Base\Database\PostType;

class Product
{
    public function create()
    {
        return PostType::create()
            ->slug('product')
            ->name(__('Product', 'natlix'))
            ->menu_icon('dashicons-products')
            ->register();
    }
}