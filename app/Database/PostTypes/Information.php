<?php

namespace App\Database\PostTypes;

use App\Base\Database\PostType;

class Information
{
    public function create()
    {
        return PostType::create()
            ->slug('information')
            ->name(__('Product Information', 'natlix'))
            ->menu_icon('dashicons-info')
            ->register();
    }
}