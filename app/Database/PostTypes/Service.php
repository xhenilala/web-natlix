<?php

namespace App\Database\PostTypes;

use App\Base\Database\PostType;

class service
{
    public function create()
    {
        return PostType::create()
            ->slug('service')
            ->name(__('Service', 'natlix'))
            ->menu_icon('dashicons-editor-ol')
            ->register();
    }
}