<?php

namespace App\Database\PostTypes;

use App\Base\Database\PostType;

class testimonial
{
    public function create()
    {
        return PostType::create()
            ->slug('testimonial')
            ->name(__('Testimonial', 'natlix'))
            ->menu_icon('dashicons-megaphone')
            ->register();
    }
}