<?php

namespace App\Database\PostTypes;

use App\Base\Database\PostType;

class Beauty
{
    public function create()
    {
        return PostType::create()
            ->slug('beauty')
            ->name(__('Beauty Tips', 'natlix'))
            ->menu_icon('dashicons-editor-help')
            ->register();
    }
}