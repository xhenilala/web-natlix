<?php

return [
    "prefix" => "nmc",

    "logs" => [
        /**
         * 
         * drivers: local, stackdriver
         * 
         */
        "driver" => "local",

        /**
         * 
         * name based on your project name
         * 
         */
        "logger_name" => "nmc",

        /**
         * 
         * only needed when you are using 'stackdriver' logs
         * 
         */
        "project_id" => "newmedia-logs",
        /**
         * 
         * json file in root of your theme directory
         * 
         */
        "credentials_file" => "wordpress-stackdriver",
    ],

    "supports" => [
        "title-tag" => null,
        "post-thumbnails" => null,
        "post-formats" => ['gallery', 'video'],
        "custom-header" => null,
        "custom-logo" => null,
        "html5" => [
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption'
        ],
    ],

    "admin_option_page" => [
        'page_title' => __('Options', 'natlix'),
        'menu_title' => __('Options', 'natlix'),
        'menu_slug' => 'options',
        'capability' => 'edit_posts',
        'redirect' => false,
        'icon_url' => 'dashicons-admin-generic',
    ],

    'nav_menus' => [
        'right-menu' => __('Right Menu', 'natlix'),
        'left-menu'  => __('Left Menu', 'natlix'),
        'primary'    => __('Primary', 'natlix'),
    ],
    "sidebars" => [
        "sidebar" => [
            'name' => __('Sidebar', 'natlix'),
            'description' => __('Default sidebar', 'natlix'),
        ],
    ],

    "assets" => [
        "prefix" => "nmc",
        "build_dir" => "build",
        "stylesheet_files" => [
            '/styles/app.css',
        ],
        "global_scripts" => [
            '/scripts/jquery.js' => [
                'key' => 'jquery',
                'in_footer' => true,
            ],
        ],
        "scripts_files" => [
            '/scripts/app.js' => [
                'key' => 'app',
                'in_footer' => true,
                'deps' => ['jquery']
            ],
        ],
        "scripts_urls" => [
            'https://maps.googleapis.com/maps/api/js' => [
                'key' => 'google-maps',
                'params' => [
                    'v' => '3.exp',
                    'key' => 'AIzaSyB42ga4NVJW73TO7Jd05o7kyldh_eL1ko4',
                ],
                'in_footer' => true,
            ],
        ],
    ],
];
