<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package NMC_Theme
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600,700,800" rel="stylesheet">
	<?php wp_head(); ?>
	<!-- <script type="text/javascript" language="JavaScript">
		
	</script> -->
</head>

<body <?php body_class(); ?>>

<div id="page" class="site d-flex flex-column">
	<?php view('parts.header'); ?>
	<?php view('parts.mobile-menu'); ?>
	<div id="content" class="site-content flex-grow-1">
